\contentsline {part}{I\hspace {1em}PART A}{4}{part.1}
\contentsline {section}{\numberline {0.1}Using Code::Blocks to compile and execute a C Program}{5}{section.1.0.1}
\contentsline {chapter}{\numberline {2}Commercial Calculator}{11}{chapter.1.2}
\contentsline {section}{\numberline {2.1}C Code}{11}{section.1.2.1}
\contentsline {chapter}{\numberline {3}Quadratic Equation}{13}{chapter.1.3}
\contentsline {section}{\numberline {3.1}C Code}{13}{section.1.3.1}
\contentsline {chapter}{\numberline {4}Palindrome Check}{15}{chapter.1.4}
\contentsline {chapter}{\numberline {5}Electricity Bill}{17}{chapter.1.5}
\contentsline {chapter}{\numberline {6}Binary Search}{19}{chapter.1.6}
\contentsline {section}{\numberline {6.1}C Code}{19}{section.1.6.1}
\contentsline {chapter}{\numberline {7}Prime Number Check}{21}{chapter.1.7}
\contentsline {section}{\numberline {7.1}C Code}{21}{section.1.7.1}
\contentsline {part}{II\hspace {1em}PART B}{23}{part.2}
\contentsline {chapter}{\numberline {7}Matrix Multiplication}{24}{chapter.2.7}
\contentsline {chapter}{\numberline {8}Compute Sine of an Angle}{28}{chapter.2.8}
\contentsline {chapter}{\numberline {9}String Operations}{30}{chapter.2.9}
\contentsline {section}{\numberline {9.1}Question}{30}{section.2.9.1}
\contentsline {chapter}{\numberline {10}Bubble Sort}{34}{chapter.2.10}
\contentsline {chapter}{\numberline {11}Square Root of a Number}{36}{chapter.2.11}
\contentsline {chapter}{\numberline {12}C Structures}{38}{chapter.2.12}
\contentsline {chapter}{\numberline {13}Pointers and Arrays}{41}{chapter.2.13}
\contentsline {chapter}{\numberline {14}Recursion}{43}{chapter.2.14}
