/***************************************************************************
*File			: A07isPrime.c
*Description	: Program to check whether the given integer is a Prime or not
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 18.04
*Date			: 16 August 2018
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

bool fnisPrime(int);
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main(void)
{
	int iVal,iFlag;
	printf("\n******************************************************");
	printf("\n*\tPROGRAM TO IMPLEMENT TEST OF PRIMALITY\t     *\n");
	printf("******************************************************");

	printf("\nEnter the value to be checked\n");
	scanf("%d",&iVal);

    iFlag = fnisPrime(iVal);
    if(iFlag)
    {
        printf("\nThe entered value %d is a prime number\n",iVal);
    }
    else
    {
        printf("\nThe entered value %d is not a prime number\n",iVal);
    }
	return 0;
}
/***************************************************************************
*Function			: fnisPrime
*Description		: Function to check whether a number is prime or not
*Input parameters	:
*	int iX	- value to be checked whether prime or not
*RETURNS			:
	true if the number is prime and	false otherwise
***************************************************************************/
bool fnisPrime(int iX)
{
	int i;
	if(1 == iX)
  	{
  	      printf("\n1 is neither prime nor composite\n");
  	      exit(0);
  	}
	for(i = 2; i*i <= iX ;i++)
	{
		if( 0 == iX % i )
		{
			return false;
		}
	}
	return true;
}
