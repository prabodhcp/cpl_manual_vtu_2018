/***************************************
*************************************************************
*	PROGRAM TO FIND ROOTS OF A QUADRATIC EQUATION	    *
*************************************************************
Enter the coefficients of a,b,c 
1 -5 6

The Roots are Real and distinct, they are 

Root1 = 3 and Root2 = 2
*************************************************************
*	PROGRAM TO FIND ROOTS OF A QUADRATIC EQUATION	    *
*************************************************************
Enter the coefficients of a,b,c 
1 4 4

Roots are equal and the Roots are 

Root1 = -2 and Root2 = -2
*************************************************************
*	PROGRAM TO FIND ROOTS OF A QUADRATIC EQUATION	    *
*************************************************************
Enter the coefficients of a,b,c 
1 3 3

The Roots are imaginary and they are

Root1 = -1.5+i0.866025		Root2 = -1.5-i0.866025
*************************************************************
*	PROGRAM TO FIND ROOTS OF A QUADRATIC EQUATION	    *
*************************************************************
Enter the coefficients of a,b,c 
0 1 2

Invalid input, not a quadratic equation - try again
***************************************/
