/***************************************************************************
*File			: B09SineAngle.c
*Description	: Program to calculate Sin(x) using Taylor series
*Author			: Prabodh C P
*Compiler		: gcc compiler, Ubuntu 18.04
*Date			: 16 August 2018
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include <math.h>
/***************************************************************************
*Function			: 	main
*Input parameters	:	no parameters
*RETURNS			:	0 on success
***************************************************************************/
int main()
{
	float fAngD, fAngR;
	float fTerm, fNum, fDen, fVal;
	int i,iNum;
	printf("\nEnter the Angle : ");	scanf("%f",&fAngD);
	printf("\nEnter the Number of terms : ");		scanf("%d",&iNum);
	printf("\nInput Angle = %g\n",fAngD);	
	printf("No of terms = %d\n",iNum);

	fAngR= (fAngD*M_PI)/180 ;
	fNum=fAngR;
	fDen=1.0;
	fVal =0.0;
	fTerm=fNum/fDen;
	for(i=1;i<=iNum;i++)
	{
		fVal = fVal + fTerm;
		fNum = -fNum * fAngR * fAngR ;
		fDen = fDen * (2*i) * (2*i+1);
		fTerm = fNum/fDen;
	}
	printf("\nCalculated value is :\nSin(%g) = %g\n",fAngD,fVal);
	printf("\nBuilt In function value is :\nSin(%g) = %g\n",fAngD, sin(fAngR));
	return 0;
}
