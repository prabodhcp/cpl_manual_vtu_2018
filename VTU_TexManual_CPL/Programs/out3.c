/***************************************
**************************************************************************
*	PROGRAM TO CHECK WHETHER AN INTEGER IS A PALINDROME OR NOT	 *
**************************************************************************
Enter a number
456

Reverse is 654
Number 456 is not a palindrome

**************************************************************************
*	PROGRAM TO CHECK WHETHER AN INTEGER IS A PALINDROME OR NOT	 *
**************************************************************************
Enter a number
45654

Reverse is 45654
Number 45654 is a palindrome

**************************************************************************
*	PROGRAM TO CHECK WHETHER AN INTEGER IS A PALINDROME OR NOT	 *
**************************************************************************
Enter a number
343

Reverse is 343
Number 343 is a palindrome


Infix Expression is (a*(b-c)/(a+b*c))

Postfix Expression is abc-*abc*+/

***************************************/

